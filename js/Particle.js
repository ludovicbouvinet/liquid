class Particle extends PIXI.Graphics {

	constructor(params) {

		super();

		this.r = params.r;

		// this.valueX = width/2 + Math.random() * (500 +500) - 500;

		// this.valueY = height/2+ Math.random() * (500 +500) - 500;

		do{

			this.valueX = Math.random() * width;

			this.valueY = Math.random() * height;


		}while(((this.valueX > (width/2) - 300 && this.valueX < (width/2) + 350) && (this.valueY > (height/2) - 350 && this.valueY < (height/2) + 350)));


		this.particle = new PIXI.Graphics();

		this.direction = {x: 0, y: 0};

		// J'ajoute le cercle a la scene

		this.addChild(this.particle);

		this.circleBody = Matter.Bodies.circle( this.valueX,  this.valueY, this.r, { mass: 0.01, frictionAir: 0, restitution:.9});


		Matter.Body.setVelocity(this.circleBody, {
			x: Common.random(-2, 2),
			y: Common.random(-2, 2)
		});

		bodies.push(this.circleBody);

		this.goal = { x: width/2, y: height/2};

		this.stop = true;

		this.draw();



	}

	draw(){


		this.particle.beginFill(0x353535, 1);

		this.particle.drawCircle(0, 0, this.r);

	}

	update(){

		if(this.stop == true && toExt == true){
			this.stop = false;
		}

		if(this.stop == false && toCenter == true){
			this.stop = true;
		}

		if(this.stop == false){
			// Matter.Vector.sub({x:this.valueX ,y:this.valueY }, this.goal, this.direction);
		}else{
			this.direction = {x: 0, y:0};
		}

		this.circleBody.position.x += 0.0005 * this.direction.x;
		this.circleBody.position.y += 0.0005 * this.direction.y;


		this.valueX = this.circleBody.position.x;
		this.valueY = this.circleBody.position.y;

		this.particle.position.x =  this.valueX;
		this.particle.position.y =  this.valueY;


	}

}