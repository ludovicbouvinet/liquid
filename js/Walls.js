class Walls extends PIXI.Container {


	constructor() {

		super();

		this.walls = [];


		var ground =  new Wall(width/2, height + height/2 , width, height, true );
		var floorTop =  new Wall(width/2, - height/2, width, height, true );
		var right =  new Wall(width + width/2, height/2 , width, height, true );
		var left =  new Wall( - width/2, height/2 , width, height, true );


		this.walls.push(ground);
		this.walls.push(floorTop);
		this.walls.push(right);
		this.walls.push(left);

		this.addChild(ground);
		this.addChild(floorTop);
		this.addChild(right);
		this.addChild(left);



	}

	update(){

		for(var i = 0; i < this.walls.length; i++){
			this.walls[i].update();
		}

	}

}