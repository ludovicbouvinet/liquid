var MatterAttractors = {
	name: 'matter-attractors',

	version: '0.1.0',

	for: 'matter-js@^0.10.0',

	install: function(base) {
		base.after('Body.create', function() {
			MatterAttractors.Body.init(this);
		});

		base.after('Engine.update', function() {
			MatterAttractors.Engine.update(this);
		});
	},

	Body: {
		init: function(body) {
			body.attractors = body.attractors || [];
		}
	},

	Engine: {
		update: function(engine) {
			var world = engine.world,
				bodies = Matter.Composite.allBodies(world);

			for (var i = 0; i < bodies.length; i += 1) {
				var bodyA = bodies[i],
					attractors = bodyA.attractors;

				if (attractors && attractors.length > 0) {
					for (var j = i + 1; j < bodies.length; j += 1) {
						var bodyB = bodies[j];

						for (var k = 0; k < attractors.length; k += 1) {
							var attractor = attractors[k],
								forceVector = attractor;

							if (Matter.Common.isFunction(attractor)) {
								forceVector = attractor(bodyA, bodyB);
							}

							if (forceVector) {
								Matter.Body.applyForce(bodyB, bodyB.position, forceVector);
							}
						}
					}
				}
			}
		}
	}
};

console.log(Matter)
Matter.Plugin.register(MatterAttractors);

if (typeof window !== 'undefined') {
	window.MatterAttractors = MatterAttractors;
}


var MatterGravity = {
	name: 'matter-gravity',

	version: '0.1.0',

	for: 'matter-js@^0.10.0',

	uses: [
		'matter-attractors@^0.1.0'
	],

	install: function(base) {
		base.after('Body.create', function() {
			MatterGravity.Body.init(this);
		});
	},

	Body: {
		init: function(body) {
			if (body.gravity) {
				body.attractors.push(MatterGravity.Body.applyGravity);
			}
		},

		applyGravity: function(bodyA, bodyB) {
			var Vector = Matter.Vector,
				Body = Matter.Body,
				bToA = Vector.sub(bodyB.position, bodyA.position),
				distanceSq = Vector.magnitudeSquared(bToA) || 0.0001,
				normal = Vector.normalise(bToA),
				magnitude = -bodyA.gravity * (bodyA.mass * bodyB.mass / distanceSq),
				force = Vector.mult(normal, magnitude);

			Body.applyForce(bodyA, bodyA.position, Vector.neg(force));
			Body.applyForce(bodyB, bodyB.position, force);
		}
	}
};

Matter.Plugin.register(MatterGravity);

if (typeof window !== 'undefined') {
	window.MatterGravity = MatterGravity;
}



var Body = Matter.Body,
	Common = Matter.Common,
	Composite = Matter.Composite;

var MatterWrap = {
	name: 'matter-wrap',

	version: '0.1.0',

	for: 'matter-js@^0.10.0',

	install: function(base) {
		base.after('Engine.update', function() {
			MatterWrap.Engine.update(this);
		});
	},

	Engine: {
		update: function(engine) {
			var world = engine.world,
				bodies = Composite.allBodies(world);

			for (var i = 0; i < bodies.length; i += 1) {
				var body = bodies[i];

				if (body.wrap) {
					MatterWrap.Body.wrap(body, body.wrap);
				}
			}
		}
	},

	Body: {
		wrap: function(body, bounds) {
			var x = null,
				y = null;

			if (typeof bounds.min.x !== 'undefined' && typeof bounds.max.x !== 'undefined') {
				if (body.bounds.min.x > bounds.max.x) {
					x = bounds.min.x - (body.bounds.max.x - body.position.x);
				} else if (body.bounds.max.x < bounds.min.x) {
					x = bounds.max.x - (body.bounds.min.x - body.position.x);
				}
			}

			if (typeof bounds.min.y !== 'undefined' && typeof bounds.max.y !== 'undefined') {
				if (body.bounds.min.y > bounds.max.y) {
					y = bounds.min.y - (body.bounds.max.y - body.position.y);
				} else if (body.bounds.max.y < bounds.min.y) {
					y = bounds.max.y - (body.bounds.min.y - body.position.y);
				}
			}

			if (x !== null || y !== null) {
				Body.setPosition(body, {
					x: x || body.position.x,
					y: y || body.position.y
				});
			}
		}
	}
};

Matter.Plugin.register(MatterWrap);

if (typeof window !== 'undefined') {
	window.MatterWrap = MatterWrap;
}

