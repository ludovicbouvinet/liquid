class Wall extends PIXI.Graphics {

	constructor(valueX,valueY, valueWidth, valueHeight, isStatic) {

		super();


		this.physicWall =  Bodies.rectangle(valueX, valueY, valueWidth, valueHeight, { isStatic: isStatic, mass: 2000, gravity: 0.001, frictionAir: 0});

		bodies.push(this.physicWall);

		this.valueX = valueX;
		this.valueY = valueY;
		this.valueWidth = valueWidth;
		this.valueHeight = valueHeight;


		this.valueOrigin = {
			x: 0,
			y: 0
		};

		this.direction = {x: 0, y: 0};

		this.goal = { x: width/2, y: height/2};

		Matter.Vector.sub(this.physicWall.position, this.goal, this.direction);

		this.norm = {x: 0, y: 0};

		this.goBack = {x: width/2 - this.valueX, y: height/2 - this.valueY};

		this.wall = new PIXI.Graphics();

		this.stop = false;

		this.addChild(this.wall);

		this.draw();



	}

	draw(){


		this.wall.beginFill(0x444334, 1);
		this.wall.drawRect(0, 0, this.valueWidth, this.valueHeight);



	}

	update(){



		this.x = this.valueOrigin.x;
		this.y = this.valueOrigin.y;

		// Matter.Vector.sub(this.physicWall.position, this.goal, this.norm);
		//
		// if(toCenter == true  && (Math.abs(this.norm.x) > 825 || Math.abs(this.norm.y) > 575)){
		//
		// 	// Matter.Vector.sub(this.physicWall.position, this.goal, this.direction);
		// 	Matter.Body.setPosition(this.physicWall, {x: this.physicWall.position.x - 0.001 * this.direction.x, y: this.physicWall.position.y - 0.001 * this.direction.y});
		//
		// 	if( !(Math.abs(this.norm.x) > 830 || Math.abs(this.norm.y) > 580)){
		//
		// 		toCenter = false;
		// 		toExt = true;
		//
		// 	}

		// }else if(toExt == true){
		//
		// 	Matter.Body.setPosition(this.physicWall, {x: this.physicWall.position.x - 0.001 * this.goBack.x, y: this.physicWall.position.y - 0.001 * this.goBack.y});
		// 	// Matter.Body.scale(physicConstraint, 1.001, 1.001);
		// }
		//
		// if( !(Math.abs(this.direction.x) > 825 || Math.abs(this.direction.y) > 575)   ){
		//
		// 	endAnimation = true;
		// 	toCenter = false;
		//
		// }

		// if(toCenter == false && endAnimation == true && toExt == false){
		//
		// 	this.direction = {x: 0, y: 0};
		//
		//
		// }

		// Matter.Body.setPosition(this.physicWall, {x: this.physicWall.position.x - 0.01 * this.direction.x, y: this.physicWall.position.y - 0.01 * this.direction.y});


		this.valueOrigin.x =  this.physicWall.position.x - this.valueWidth/2;
		this.valueOrigin.y =  this.physicWall.position.y - this.valueHeight/2;





 		// Matter.Body.scale(this.physicWall, 0.5, 0.5)




	}

}