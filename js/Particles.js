class Particles extends PIXI.Container {


	constructor(params, nbParticles) {

		super();

		this.particles = [];

		for(var i = 0; i < nbParticles; i++ ){
			var particle = new Particle(params);
			this.particles.push(particle);
			this.addChild(particle);
		}





	}

	update(){


		for(var i = 0; i < this.particles.length; i ++){
			this.particles[i].update();
		}



		// this.particle.filters =[new PIXI.filters.BlurXFilter(), new PIXI.filters.BlurYFilter()];
		// this.particle.filters =[filter];

		// this.draw();

	}

}