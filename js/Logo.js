class Logo extends PIXI.Container {


	constructor() {

		super();


		var texture = PIXI.Texture.fromImage('./src/logo.png');

		this.img = new PIXI.Sprite(texture);

		this.valueAlpha = 0;

		this.img.anchor.x = 0.5;
		this.img.anchor.y = 0.5;

		this.img.alpha = this.valueAlpha;

		this.img.position.x = width/2;
		this.img.position.y = height/2;


		this.addChild(this.img);
	}

	update(){

		if(endAnimation == true && this.valueAlpha < 1){
			this.valueAlpha += 0.005;
			this.img.alpha = this.valueAlpha;
		}

		if(this.valueAlpha > 0.95){
			toExt = true;
		}


	}

}