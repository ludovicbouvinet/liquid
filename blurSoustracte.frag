#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

#define PROCESSING_TEXTURE_SHADER

varying vec2 vTextureCoord;
uniform sampler2D uSampler;
uniform float threshold;

void main(void)
{
	vec3 col = texture2D(uSampler, vTextureCoord).rgb;
	float bright =  0.9 * (col.r + col.g + col.b);
	float b = mix(0.0, 1.0, step(threshold, bright));
    gl_FragColor = vec4(vec3(b), 1.0);
}